import React, { useEffect, useState } from "react";
import "./App.css";
import Recipe from "./Recipe";
import Header from "./Components/Header/Header";
import { APP_KEY } from "./constants";

const App = () => {
  const [recipes, setRecipes] = useState([]);
  const [search, setSearch] = useState("");
  const [query, setQuery] = useState("chicken");
  useEffect(() => {
    getRecipes();
  }, [query]);
  const getRecipes = async () => {
    const response = await fetch(
      `https://api.spoonacular.com/recipes/complexSearch?apiKey=${APP_KEY}&query=${query}`
    );
    const data = await response.json();
    setRecipes(data.results);
    // console.log(data);
  };
  const updateSearch = (e) => {
    setSearch(e.target.value);
  };
  const getSearch = (e) => {
    e.preventDefault();
    setQuery(search);
    setSearch("");
  };

  return (
    <div className="App">
      <Header search={search} updateSearch={updateSearch} getSearch={getSearch} />
      <div className="recipes">
        {recipes.map((recipe) => (
          <Recipe
            key={recipe.title}
            title={recipe.title}
            image={recipe.image}
          />
        ))}
      </div>
    </div>
  );
};

export default App;
